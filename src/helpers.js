import {getCookie} from "./cookie";
import * as api from './api';

const auth = () => !!getCookie('token');
export const FAVOURITES_KEY = 'favourites';
export const FAVOURITE_PRODUCTS_KEY = 'favourite_products';
export const CART_KEY = 'cart';
export const CART_PRODUCTS_KEY = 'cart_products';

export const set_favourite = product => {
  if (auth()) {
    api.setFavourite([product.id]);
  } else {
    localStorage.setItem(FAVOURITES_KEY, JSON.stringify(
      (() => {
        if (!localStorage.getItem(FAVOURITES_KEY))
          return [product.id];

        const favourites = new Set(JSON.parse(localStorage.getItem(FAVOURITES_KEY)));
        favourites.add(product.id);
        return Array.from(favourites);
      })()
    ));

    localStorage.setItem(FAVOURITE_PRODUCTS_KEY, JSON.stringify(
      (() => {
        if (product.images)
          product.image = product.images[0] ? product.images[0] : null;

        if (!localStorage.getItem(FAVOURITE_PRODUCTS_KEY))
          return [product];

        const favouriteProducts = new Set(JSON.parse(localStorage.getItem(FAVOURITE_PRODUCTS_KEY)));
        favouriteProducts.add(product);
        return Array.from(favouriteProducts);
      })()
    ));
  }
}

export const unset_favourite = product => {
  if (auth()) {
    api.unsetFavourite([product.id]);
  } else {
    localStorage.setItem(FAVOURITES_KEY, JSON.stringify(
      (() => {
        if (!localStorage.getItem(FAVOURITES_KEY))
          return [];

        const favourites = new Set(JSON.parse(localStorage.getItem(FAVOURITES_KEY)));
        favourites.delete(product.id);
        return Array.from(favourites);
      })()
    ));

    localStorage.setItem(FAVOURITE_PRODUCTS_KEY, JSON.stringify(
      (() => {
        if (!localStorage.getItem(FAVOURITE_PRODUCTS_KEY))
          return [];

        const favouriteProducts = JSON.parse(localStorage.getItem(FAVOURITE_PRODUCTS_KEY));
        return favouriteProducts.filter(item => item.id !== product.id);
      })()
    ));
  }
}

export const edit_cart = (product, count) => {
  if (auth()) {
    api.editCart([{
      product_id: product.id,
      count: count
    }]);
  } else {
    localStorage.setItem(CART_KEY, JSON.stringify(
      (() => {
        if (!localStorage.getItem(CART_KEY))
          return count === 0 ? [] : [product.id];

        const cart = new Set(JSON.parse(localStorage.getItem(CART_KEY)));

        if (count === 0) {
          cart.delete(product.id);
        } else {
          cart.add(product.id);
        }

        return Array.from(cart);
      })()
    ));

    localStorage.setItem(CART_PRODUCTS_KEY, JSON.stringify(
      (() => {
        if (product.images)
          product.image = product.images[0] ? product.images[0] : null;

        const data = {...product, count: count};

        if (!localStorage.getItem(CART_PRODUCTS_KEY))
          return count === 0 ? [] : [data];

        const cartProducts = JSON.parse(localStorage.getItem(CART_PRODUCTS_KEY));

        if (!cartProducts.find(item => item.id === product.id))
          cartProducts.push(data);

        return count === 0
          ? cartProducts.filter(item => item.id !== product.id)
          : cartProducts.map(item => {
            if (item.id === product.id)
              item.count = count;

            return item;
          });
      })()
    ));
  }
}

export const get_local = key => {
  if (!localStorage.getItem(key))
    return [];
  return JSON.parse(localStorage.getItem(key));
}

