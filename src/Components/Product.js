import React from 'react';

// CSS
import '../Assets/css/product.css';

const Product = ({product, preview, thumbnails, handleThumbnailClick, handleToCartClick, handleToFavouritesClick, inCart, inFavourite}) => {
  const cartButton = (
    <button onClick={() => handleToCartClick(product)}
            className={inCart ? 'btn btn-dark mt-5 d-block' : 'btn btn-outline-dark mt-5 d-block'}>
      {inCart ? 'Убрать из корзины' : 'В корзину'}
    </button>
  );

  const favouritesButton = (
    <button onClick={() => handleToFavouritesClick(product)}
            className={inFavourite ? 'btn btn-dark mt-5 d-block' : 'btn btn-outline-dark mt-5 d-block'}>
      {inFavourite ? 'Убрать из избранного' : 'В избранное'}
    </button>
  );
  return (
    <main className="product row my-lg-5">
      <div className="col-md-6 col-sm-12 d-flex justify-content-center">
        <div>
          <div className="main-image-container">
            <img src={preview} alt="Товар"/>
          </div>
          <ul className="mt-3 img-thumbnail-list">
            {
              thumbnails.map(
                (image, index) =>
                  <li onClick={() => handleThumbnailClick(image)} key={`image_${index}`}>
                    <img src={image} alt="Товар" className="img-thumbnail"/>
                  </li>
              )
            }
          </ul>
        </div>
      </div>
      <div className="col-md-6 col-sm-12 p-5">
        <h2>{product.price} &#8381;</h2>
        <h1 className="my-5 text-uppercase">{product.name}</h1>
        <p>
          {product.description}
        </p>
        {cartButton}
        {favouritesButton}
      </div>
    </main>
  );
}

export default Product;