import React from 'react';
import {Link} from "react-router-dom";

const CartCard = ({product, price, increment, decrement, handleCountChange}) => {
  const productPath = '/product/' + product.id;
  const incrementIcon = (
    <svg className="bi bi-plus-circle" width="1em" height="1em" viewBox="0 0 16 16"
         fill="currentColor"
         xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd"
            d="M8 3.5a.5.5 0 01.5.5v4a.5.5 0 01-.5.5H4a.5.5 0 010-1h3.5V4a.5.5 0 01.5-.5z"
            clipRule="evenodd"/>
      <path fillRule="evenodd" d="M7.5 8a.5.5 0 01.5-.5h4a.5.5 0 010 1H8.5V12a.5.5 0 01-1 0V8z"
            clipRule="evenodd"/>
      <path fillRule="evenodd" d="M8 15A7 7 0 108 1a7 7 0 000 14zm0 1A8 8 0 108 0a8 8 0 000 16z"
            clipRule="evenodd"/>
    </svg>
  );
  const decrementIcon = product.count <= 1
    ? (
      <svg className="bi bi-x-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
           xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" d="M8 15A7 7 0 108 1a7 7 0 000 14zm0 1A8 8 0 108 0a8 8 0 000 16z"
              clipRule="evenodd"/>
        <path fillRule="evenodd" d="M11.854 4.146a.5.5 0 010 .708l-7 7a.5.5 0 01-.708-.708l7-7a.5.5 0 01.708 0z"
              clipRule="evenodd"/>
        <path fillRule="evenodd" d="M4.146 4.146a.5.5 0 000 .708l7 7a.5.5 0 00.708-.708l-7-7a.5.5 0 00-.708 0z"
              clipRule="evenodd"/>
      </svg>
    )
    : (
      <svg className="bi bi-dash-circle" width="1em" height="1em" viewBox="0 0 16 16"
           fill="currentColor"
           xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" d="M8 15A7 7 0 108 1a7 7 0 000 14zm0 1A8 8 0 108 0a8 8 0 000 16z"
              clipRule="evenodd"/>
        <path fillRule="evenodd" d="M3.5 8a.5.5 0 01.5-.5h8a.5.5 0 010 1H4a.5.5 0 01-.5-.5z"
              clipRule="evenodd"/>
      </svg>
    );
  return (
    <div className="card mb-3">
      <div className="row">
        <Link to={productPath} className="link-without-style col-md-3 col-12 p-0">
          <img src={product.image} alt="Товар" className="w-100"
               style={{objectFit: 'cover'}}/>
        </Link>
        <div className="col-lg-9 col-12 p-2">
          <div className="row">
            <Link to={productPath} className="link-without-style col-md-4 col-sm-8">
              <h4>{product.name}</h4>
              <p>{product.description}</p>
            </Link>
            <div className="col-md-4 col-sm-12 d-flex align-items-center flex-lg-row flex-column flex-column-reverse">
              <button onClick={decrement} type="button" className="counter-btn btn btn-outline-dark">
                {decrementIcon}
              </button>
              <input type="text"
                     name={product.id}
                     className="form-control border-dark"
                     value={product.count}
                     onChange={handleCountChange}/>
              <button onClick={increment} type="button" className="counter-btn btn btn-outline-dark">
                {incrementIcon}
              </button>
            </div>
            <div
              className="col-md-4 col-sm-12 d-flex align-items-center justify-content-lg-end mt-lg-0 mt-3 pr-5">
              <h4>{price}Р</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CartCard;