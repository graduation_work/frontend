import React from 'react';

import '../Assets/css/not-found.css';

export default function NotFound() {
  return <div className="not-found d-flex justify-content-center align-items-center flex-column w-100 h-100">
    <div style={{fontSize: '15em', color: '#ccc'}}>404</div>
    <div style={{fontSize: '2em', color: '#ccc'}}>страница не найдена</div>
  </div>;
}