import React from 'react';

// CSS
import '../Assets/css/pagination.css';

const Pagination = ({activePage, pagesCount, handlePageClick}) => {
  if (pagesCount <= 1)
    return null;

  const pageButton = (number) => (
    <li key={'pagination_' + number} className={number === activePage ? 'page-item active' : 'page-item'}>
      <button onClick={() => handlePageClick(number)} className="page-link">{number}</button>
    </li>
  );

  const pagination = [];

  for (let i = 1; i <= pagesCount; i++) {
    pagination.push(pageButton(i));
  }

  return (
    <div className="d-flex justify-content-center">
      <nav>
        <ul className="pagination">{pagination}</ul>
      </nav>
    </div>
  );
}

export default Pagination;