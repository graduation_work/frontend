import React from 'react';
import {CSSTransition, TransitionGroup} from "react-transition-group";

// COMPONENTS
import ProductCard from "./ProductCard";
import Pagination from "./Pagination";

const ShopProducts = ({products, handlePageClick, currentPage, lastPage}) => {
  return (
    <section>
      <TransitionGroup className="d-flex flex-wrap justify-content-center">
        {
          products.map(
            product => (
              <CSSTransition
                key={product.id}
                timeout={500}
                classNames="item"
              >
                <ProductCard
                  key={'shop_product_card_' + product.id}
                  product={product}
                  className="m-3"
                />
              </CSSTransition>
            )
          )
        }
      </TransitionGroup>

      <Pagination activePage={currentPage} pagesCount={lastPage} handlePageClick={handlePageClick}/>
    </section>
  )
};

export default ShopProducts;

