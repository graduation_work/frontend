import React from 'react';

// CSS
import '../Assets/css/sidebar.css';

const Sidebar = (
  {
    hidden,
    categories,
    filters,
    handleCategoryChange,
    handlePriceClick, handleSearchClick,
    handleSortChange,
    resetFilters
  }
) => {
  const className = 'bg-light' + (hidden ? '' : ' toggled');

  return (
    <div className={className} id="sidebar">
      <h3>Сортировка</h3>
      <div className="group">
        <div className="input-group">
          <select onChange={e => handleSortChange(e.target.value)} className="custom-select" name="sidebarSort"
                  id="sidebarSort" value={!!filters.sort ? filters.sort : ''}>
            <option value="name">По алфавиту</option>
            <option value="price">По цене</option>
          </select>
        </div>
      </div>

      <h3>Поиск</h3>
      <div className="group">
        <div className="input-group">
          <input id="filterName" type="text" className="form-control"
                 defaultValue={!!filters.name ? filters.name : ''}/>
          <button onClick={handleSearchClick} className="btn btn-outline-dark ml-1">Поиск</button>
        </div>
      </div>

      <h3>Цена</h3>
      <div className="group">
        <div className="input-group">
          <input id="filterPriceFrom" type="number" className="form-control mr-1"
                 defaultValue={!!filters.priceFrom ? filters.priceFrom : ''}/>
          <span className="mt-1"> - </span>
          <input id="filterPriceTo" type="number" className="form-control ml-1"
                 defaultValue={!!filters.priceTo ? filters.priceTo : ''}/>
        </div>
        <button onClick={handlePriceClick} className="btn btn-outline-dark mt-1" style={{width: '100%'}}>
          Применить
        </button>
      </div>

      <h3>Категория</h3>
      <div className="group">
        <div className="input-group">
          <select onChange={e => handleCategoryChange(e.target.value)} className="custom-select" name="sidebarCategory"
                  id="sidebarCategory" value={!!filters.categoryId ? filters.categoryId : 0}>
            <option key={'category_' + 0} value={0} disabled={true}>
              Выберите категорию
            </option>
            {
              categories.map(category =>
                <option key={'category_' + category.id} value={category.id}>
                  {category.name}
                </option>
              )
            }
          </select>
        </div>
      </div>


      <div className="group">
        <div className="input-group">
          <button onClick={resetFilters} className="btn btn-dark mt-3 w-100 text-uppercase">Сбросить фильтры</button>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;