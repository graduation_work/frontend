import React from 'react';
import {CSSTransition, TransitionGroup} from "react-transition-group";
import Empty from "./Empty";

// COMPONENTS
import ProductCard from "./ProductCard";
import Pagination from "./Pagination";

const Favourites = ({products, currentPage, lastPage, handlePageClick, handleFavouritesButtonClick}) => {
    const content = () => {
      if (products.length === 0) {
        return <Empty/>;
      }

      return (
        <TransitionGroup className="d-flex flex-wrap justify-content-between">
          {
            products.map(
              product => (
                <CSSTransition
                  key={product.id}
                  timeout={500}
                  classNames="item"
                >
                  <ProductCard
                    key={`favourite_${product.id}`}
                    product={product}
                    className="mb-3"
                    handleToFavouritesClick={handleFavouritesButtonClick}
                  />
                </CSSTransition>
              )
            )
          }
        </TransitionGroup>
      );
    }

    return (
      <main className="px-5">
        <h2 className="py-4">ИЗБРАННОЕ</h2>
        {content()}
        <Pagination activePage={currentPage} pagesCount={lastPage} handlePageClick={handlePageClick}/>
      </main>
    );
  }
;

export default Favourites;