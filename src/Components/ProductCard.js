import React from "react";
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import {edit_cart, set_favourite, unset_favourite} from '../helpers';
import _ from "lodash";

// REDUX ACTIONS
import {setFavourite, unsetFavourite, addCartProduct, removeCartProduct} from "../Redux/Actions";

// CSS
import '../Assets/css/product-card.css';

class ProductCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cart: false,
      favourite: false,
    };
  }

  handleToCartClick = product => {
    if (this.props.cart.indexOf(product.id) !== -1) {
      this.props.removeFromCart(product.id);
      edit_cart(product, 0);
    } else {
      this.props.addToCart(product.id);
      edit_cart(product, 1);
    }
  }

  handleToFavouritesClick = product => {
    if (this.props.favourites.indexOf(product.id) !== -1) {
      this.props.unsetFavourite(product.id);
      unset_favourite(product);
    } else {
      this.props.setFavourite(product.id);
      set_favourite(product);
    }
  }

  componentDidMount() {
    this.setState({
      cart: this.props.cart.indexOf(this.props.product.id) !== -1,
      favourite: this.props.favourites.indexOf(this.props.product.id) !== -1,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const favourite = this.props.favourites.indexOf(this.props.product.id) !== -1;
    const cart = this.props.cart.indexOf(this.props.product.id) !== -1;

    const newState = {};

    if (prevState.favourite !== favourite)
      newState.favourite = favourite;
    if (prevState.cart !== cart)
      newState.cart = cart;
    if (!_.isEmpty(newState))
      this.setState(newState);
  }

  render() {
    const {product, className} = this.props;

    const path = `/product/${product.id}`;

    const cartButton = (
      <button onClick={() => this.handleToCartClick(product)}
              className={this.state.cart ? 'btn btn-dark px-1' : 'btn btn-outline-dark'}>
        {this.state.cart ? 'Убрать из корзины' : 'В корзину'}
      </button>
    );

    const favouritesButton = (
      <button onClick={() => this.handleToFavouritesClick(product)}
              className={this.state.favourite ? 'btn btn-dark px-1' : 'btn btn-outline-dark'}>
        {this.state.favourite ? 'Убрать из избранного' : 'В избранное'}
      </button>
    );

    return (
      <div className={className + " card product-card"}>
        <Link to={path}>
          <img className="card-img-top" src={product.image} alt="Товар"/>
        </Link>
        <div className="card-body">
          <Link to={path}>
            <h5 className="card-title d-flex justify-content-between">
              <span>{product.name}</span>
              <span><u>{product.price}Р</u></span>
            </h5>
            <p className="card-text">{product.description}</p>
          </Link>
          <div className="d-flex justify-content-between mt-3">
            {cartButton}
            {favouritesButton}
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    cart: state.cart,
    favourites: state.favourites
  }),
  dispatch => ({
    setFavourite: id => dispatch(setFavourite(id)),
    unsetFavourite: id => dispatch(unsetFavourite(id)),
    addToCart: id => dispatch(addCartProduct(id)),
    removeFromCart: id => dispatch(removeCartProduct(id))
  })
)(ProductCard);