import React from 'react';
import {CSSTransition, TransitionGroup} from "react-transition-group";
import Empty from "./Empty";

// COMPONENTS
import Pagination from "./Pagination";

const Orders = ({orders, currentPage, lastPage, handlePageClick}) => {
  const content = () => {
    if (orders.length === 0) {
      return <Empty/>;
    }

    return (
      <TransitionGroup className="col-12 p-0">
        {
          orders.map(
            order => {
              const date = (new Date(order.created_at)).toLocaleDateString('ru-RU');
              const content = JSON.parse(order.content);

              return (
                <CSSTransition
                  key={order.id}
                  timeout={500}
                  classNames="item"
                >
                    <div className="card mb-3">
                      <div className="card-header border-bottom-0 d-flex justify-content-between align-items-center">
                        <h4>Заказ #{order.id}</h4>
                        <button
                          className="btn p-0 m-0"
                          data-toggle="collapse"
                          data-target={`#collapse_${order.id}`}
                        >
                          (подробности)
                        </button>
                      </div>
                      <div className="card-body collapse" id={`collapse_${order.id}`}>
                        <ul className="list-group">
                          {
                            content.map(item => (
                              <li
                                key={`content_item_${item.id}`}
                                className="list-group-item"
                              >
                                <h5>{item.name}</h5>
                                <div>{item.description}</div>
                                <div>Цена: {item.price} Р</div>
                                <div>Количество: {item.count} шт.</div>
                              </li>
                            ))
                          }
                        </ul>
                      </div>
                      <div className="card-footer row px-0 border-top-0">
                        <div className="col-lg-6 col-12">Сумма заказа: {order.price} Р</div>
                        <div className="col-lg-6 col-12 text-lg-right">Дата оформления: {date}</div>
                      </div>
                    </div>
                </CSSTransition>
              );
            }
          )
        }
      </TransitionGroup>
    );
  }

  return (
    <main className="px-5">
      <h2 className="py-4 text-uppercase">Мои заказы</h2>
      <div className="row">
        {content()}
      </div>
      <Pagination activePage={currentPage} pagesCount={lastPage} handlePageClick={handlePageClick}/>
    </main>
  );
};

export default Orders;