import React from 'react';
import {Link} from 'react-router-dom';

// CSS
import '../Assets/css/home-card.css';

const Home = ({products}) => (
  <main className="d-flex row m-0">
    {
      products.map(
        ({id, image, name, price}) => (
          <div key={'home_product_' + id} className="col-lg-3 col-md-6 col-sm-12 p-0 image-container">
            <Link to={'/product/' + id}>
              <img src={image} alt="Товар"/>
              <div className="bottom-right">
                <span className="text-uppercase">{name}</span><br/>
                <span>{price}Р</span><br/>
              </div>
            </Link>
          </div>
        )
      )
    }
  </main>
);

export default Home;