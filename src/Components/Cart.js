import React from 'react';
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import Empty from "./Empty";

// CSS
import '../Assets/css/cart.css';

// COMPONENTS
import CartCard from "./CartCard";

const Cart = (props) => {
  const content = props.products.reduce((acc, product) => {
    const currentProductPrice = product.count * product.price;

    acc.totalPrice += currentProductPrice;

    acc.products.push(
      <CSSTransition
        key={product.id}
        timeout={500}
        classNames="item"
      >
        <CartCard
          product={product}
          increment={() => props.handleCountChange(product, product.count + 1)}
          decrement={() => props.handleCountChange(product, product.count - 1)}
          handleCountChange={(e) => props.handleCountChange(product, e.target.value)}
          price={currentProductPrice}
        />
      </CSSTransition>
    );

    acc.list.push(
      <li key={'total-item-' + product.id} className="list-group-item d-flex justify-content-between">
        <span>{product.name}</span>
        <span>{currentProductPrice}Р</span>
      </li>
    );

    return acc;
  }, {
    products: [],
    list: [],
    totalPrice: 0,
  });

  const form = () => {
    if (props.auth) {
      return <button
        onClick={props.handleBuyClick}
        className="btn btn-dark w-100 text-uppercase"
        type="submit"
      >
        {props.orderInProcess ? 'Пожалуйста, подождите...' : 'Оформить заказ'}
      </button>
    }

    return <form onSubmit={e => e.preventDefault()}>
      <div className="form-group row w-100 pt-2 d-flex align-items-center">
        <label
          htmlFor="email"
          className="col-3 col-form-label text-uppercase text-left px-0"
          style={{fontSize: '1.25rem'}}
        >
          Email
        </label>
        <input type="text" className="form-control col-9" id="email" required={true}/>
        <small className="form-text text-muted m-0 p-0">
          Будет использоваться для связи.
          Сюда будут присланы детали заказа.
        </small>
      </div>
      <button
        onClick={props.handleBuyClick}
        className="btn btn-dark w-100 text-uppercase"
        type="submit"
      >
        {props.orderInProcess ? 'Пожалуйста, подождите...' : 'Оформить заказ'}
      </button>
    </form>
  };

  return (
    <main className="cart container-fluid px-5 mb-3">
      <h2 className="py-4">КОРЗИНА</h2>
      <div className="row">
        <div className="col-md-8 col-12">
          {
            content.products.length > 0
              ? <TransitionGroup>{content.products}</TransitionGroup>
              : <Empty/>
          }
        </div>
        <div className="total col-md-4 col-12 py-3 bg-light">
          <div className="h-100 d-flex flex-column justify-content-between">
            <h3>Заказ ({content.products.length})</h3>
            <ul className="list-group">
              {content.list}
            </ul>
            <div className="d-flex justify-content-between">
              <span>ИТОГО</span>
              <span>{content.totalPrice}P</span>
            </div>
            {form()}
          </div>
        </div>
      </div>
    </main>
  );
}

export default Cart;