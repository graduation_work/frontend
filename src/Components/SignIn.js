import React from 'react';
import {Link} from "react-router-dom";

const SignIn = ({handleSignInClick, invalidCredentials}) => {
  const formControl = invalidCredentials ? 'form-control is-invalid' : 'form-control';

  return (
    <main className="container">
      <div className="row vh-100 d-flex align-items-center">
        <form className="col-lg-4 col-12 m-auto bg-light p-5" onSubmit={e => e.preventDefault()}>
          <h2 className="pb-3">ВХОД</h2>
          <div className="mb-3">
            <label htmlFor="email" className="d-block">ЭЛЕКТРОННАЯ ПОЧТА</label>
            <input id="email" type="text" className={formControl} placeholder="user@email.com" required={true}/>
            <div className="invalid-feedback">Неправильный логин/пароль</div>
          </div>
          <div className="mb-3">
            <label htmlFor="password" className="d-block">ПАРОЛЬ</label>
            <input id="password" type="password" className={formControl} required={true}/>
            <small className="form-text text-muted" title="В данный момент эта функция недоступна">Забыли
              пароль?</small>
          </div>
          <button onClick={handleSignInClick} className="btn btn-dark w-100" type="submit">ВОЙТИ</button>
          <small className="form-text text-muted "><Link to="/signup" className="link-without-style">Нет аккаунта?
            Зарегистрироваться</Link></small>
        </form>
      </div>
    </main>
  );
}

export default SignIn;