import React from 'react';

export default function Empty() {
  return <div className="d-flex justify-content-center align-items-center w-100 h-100">
    <span style={{fontSize: '2em', color: '#ccc', padding: '2em 0'}}>Здесь ничего нет :(</span>
  </div>;
}