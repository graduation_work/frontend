import React from 'react';
import {Link, NavLink} from 'react-router-dom';
import '../Assets/css/header.css';

const Header = ({cartCount, favouritesCount, profile, signOut}) => (
  <header>
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to="/">
        <span className="logo left">Shop</span>
        <span className="logo right">Top</span>
      </Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"/>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <NavLink className="nav-link text-uppercase" to="/" exact>Главная</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link text-uppercase" to="/shop" exact>Магазин</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link text-uppercase" to="/cart" exact>Корзина ({cartCount})</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link text-uppercase" to="/favourites" exact>Избранное ({favouritesCount})</NavLink>
          </li>
          {
            !profile.authorized ? null :

              <li className="nav-item">
                <NavLink className="nav-link text-uppercase" to="/orders" exact>Мои заказы</NavLink>
              </li>
          }
        </ul>

        <div className="form-inline my-2 my-lg-0">
          {
            profile.authorized
              ? (
                <React.Fragment>
                  <span className="mr-2">{profile.email}</span>
                  <button onClick={signOut} className="btn btn-outline-dark">ВЫХОД</button>
                </React.Fragment>
              )
              : (
                <React.Fragment>
                  <Link to="/signin" className="btn btn-outline-dark my-2 my-sm-0 mr-1 text-uppercase">Вход</Link>
                  <Link to="/signup" className="btn btn-dark my-2 my-sm-0 text-uppercase">Регистрация</Link>
                </React.Fragment>
              )
          }
        </div>

      </div>
    </nav>
  </header>
);

export default Header;