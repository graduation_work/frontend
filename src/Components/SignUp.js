import React from 'react';
import {Link} from "react-router-dom";

const SignUp = ({errors, handleSignUpClick}) => (
  <main className="container">
    <div className="row vh-100 d-flex align-items-center">
      <form className="col-lg-4 col-12 m-auto bg-light p-5" onSubmit={e => e.preventDefault()}>
        <h2 className="pb-3">РЕГИСТРАЦИЯ</h2>
        <div className="mb-3">
          <label form="email" className="d-block">ЭЛЕКТРОННАЯ ПОЧТА</label>
          <input id="email" type="text" className={errors.email ? 'form-control is-invalid' : 'form-control'}
                 placeholder="user@email.com" required={true}/>
          <div className="invalid-feedback">{errors.email ? errors.email[0] : null}</div>
        </div>
        <div className="mb-3">
          <label form="password" className="d-block">ПАРОЛЬ</label>
          <input id="password" type="password" className={errors.password ? 'form-control is-invalid' : 'form-control'}
                 required={true}/>
        </div>
        <div className="mb-3">
          <label htmlFor="password_confirmation" className="d-block">ПОВТОРИТЕ ПАРОЛЬ</label>
          <input id="password_confirmation" type="password"
                 className={errors.password ? 'form-control is-invalid' : 'form-control'} required={true}/>
          <div
            className="invalid-feedback">{errors.password ? errors.password[0] : null}</div>
        </div>
        <button onClick={handleSignUpClick} className="btn btn-dark w-100 mt-2" type="submit">РЕГИСТРАЦИЯ</button>
        <small className="form-text text-muted "><Link to="/signin" className="link-without-style">Уже зарегистрированы?
          Войти</Link></small>
      </form>
    </div>
  </main>
);

export default SignUp;