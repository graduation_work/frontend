import axios from 'axios';
import {getCookie} from "./cookie";

export const setToken = token => axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

const production = 'https://api.vladlengil.tk/';
const local = 'http://localhost:8020/';

const host = (type) => {
  switch (type) {
    case 'local':
      return local;
    case 'production':
      return production;
    default:
      return 'http://localhost';
  }
}

const environmentUrl = host('production');

axios.defaults.baseURL = environmentUrl + 'api/';
axios.defaults.headers.common['Accept'] = 'application/json';

const token = getCookie('token')
if (token) setToken(token);

export const getProducts = (params) => axios.get('products', {params: params});
export const getRandomProducts = () => axios.get('products/random');
export const showProduct = (id) => axios.get('products/' + id)

export const getCategories = () => axios.get('categories');

export const getCart = () => axios.get('cart');
/**
 *
 * @param products = [
 *     {
 *        id: 1,
 *        count: 3,
 *     },
 *     {
 *        id: 2,
 *        count: 0,
 *     },
 * ];
 * @returns {Promise<AxiosResponse<any>>}
 */
export const editCart = (products) => axios.post('cart', {products: products});

export const getFavourites = (page = 1) => axios.get('favourites', {params: {page}});
/**
 *
 * @param products = [1, 2, 3];
 * @returns {Promise<AxiosResponse<any>>}
 */
export const setFavourite = (products) => axios.post('favourites', {products: products});
/**
 *
 * @param products = [1, 2, 3];
 * @returns {Promise<AxiosResponse<any>>}
 */
export const unsetFavourite = (products) => axios.delete('favourites', {
  params: {
    products: products
  }
});

export const getProfile = () => axios.get('me');
export const signIn = (email, password) => axios.post('signin', {
  email: email,
  password: password
});
export const signUp = (email, password, passwordConfirmation) => axios.post('signup', {
  email: email,
  password: password,
  password_confirmation: passwordConfirmation
});

export const getOrders = (page) => axios.get('orders');
export const createOrder = (order = null, email = null) => axios.post('orders', {
  order: order,
  email: email,
});
