import React from 'react';
import {connect} from 'react-redux';
import * as api from '../api';
import _ from 'lodash';
import {edit_cart, set_favourite, unset_favourite} from "../helpers";

// REDUX ACTIONS
import {
  addCartProduct,
  removeCartProduct,
  setFavourite,
  unsetFavourite
} from "../Redux/Actions";

// COMPONENTS
import Product from "../Components/Product";

class ProductContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cart: false,
      favourite: false,
      product: {},
      preview: null,
      thumbnails: [],
    }
  }

  handleThumbnailClick = image => this.setState({preview: image});

  handleToCartClick = product => {
    if (this.props.cart.indexOf(product.id) !== -1) {
      this.props.removeFromCart(product.id);
      edit_cart(product, 0);
    } else {
      this.props.addToCart(product.id);
      edit_cart(product, 1);
    }
  }

  handleToFavouritesClick = product => {
    if (this.props.favourites.indexOf(product.id) !== -1) {
      this.props.unsetFavourite(product.id);
      unset_favourite(product);
    } else {
      this.props.setFavourite(product.id);
      set_favourite(product);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const favourite = this.props.favourites.indexOf(this.state.product.id) !== -1;
    const cart = this.props.cart.indexOf(this.state.product.id) !== -1;

    const newState = {};

    if (prevState.favourite !== favourite)
      newState.favourite = favourite;
    if (prevState.cart !== cart)
      newState.cart = cart;
    if (!_.isEmpty(newState))
      this.setState(newState);
  }

  componentDidMount() {
    api.showProduct(this.props.match.params.id).then(response => this.setState({
      product: response.data,
      preview: response.data.images[0] ? response.data.images[0] : null,
      thumbnails: response.data.images
    }));
  }

  render() {
    return <Product
      product={this.state.product}
      preview={this.state.preview}
      thumbnails={this.state.thumbnails}
      handleThumbnailClick={this.handleThumbnailClick}
      handleToCartClick={this.handleToCartClick}
      handleToFavouritesClick={this.handleToFavouritesClick}
      inCart={this.state.cart}
      inFavourite={this.state.favourite}
    />;
  }
}

export default connect(
  state => ({
    favourites: state.favourites,
    cart: state.cart,
  }),
  dispatch => ({
    setFavourite: id => dispatch(setFavourite(id)),
    unsetFavourite: id => dispatch(unsetFavourite(id)),
    addToCart: id => dispatch(addCartProduct(id)),
    removeFromCart: id => dispatch(removeCartProduct(id))
  })
)
(ProductContainer);