import React from 'react';
import * as api from '../api';

// COMPONENTS
import Home from "../Components/Home";

class HomeContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      products: []
    };
  }

  componentDidMount() {
    api.getRandomProducts().then(response => this.setState({products: response.data}));
  }

  render() {
    return <Home products={this.state.products}/>
  }
}

export default HomeContainer;