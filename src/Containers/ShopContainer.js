import React from 'react';
import {connect} from 'react-redux';
import * as api from '../api';
import {getCategories} from "../Redux/Actions";
import _ from 'lodash';

// COMPONENTS
import Sidebar from "../Components/Sidebar";
import ShopProducts from "../Components/ShopProducts";

class ShopContainer extends React.Component {
  constructor(props) {
    super(props);

    const isMobile = window.innerWidth < 768
    const queryParams = (new URLSearchParams(props.location.search));
    const queryFilters = {
      page: parseInt(queryParams.get('page'), 10),
      priceFrom: parseInt(queryParams.get('price_from'), 10),
      priceTo: parseInt(queryParams.get('price_to'), 10),
      categoryId: parseInt(queryParams.get('category_id'), 10),
      sort: queryParams.get('sort'),
      name: queryParams.get('name'),
    };

    this.state = {
      isMobile: isMobile,
      sidebarHidden: isMobile,
      products: [],
      currentPage: queryFilters.page ? queryFilters.page : 1,
      lastPage: null,
      filters: {
        categoryId: queryFilters.categoryId ? queryFilters.categoryId : null,
        name: queryFilters.name ? queryFilters.name.trim() : null,
        priceFrom: queryFilters.priceFrom ? queryFilters.priceFrom : null,
        priceTo: queryFilters.priceTo ? queryFilters.priceTo : null,
        sort: queryFilters.sort ? queryFilters.sort.trim() : null,
      }
    }
  }

  handleToggleFiltersClick = () => this.setState({sidebarHidden: !this.state.sidebarHidden});

  handleResize = () => {
    const isMobile = window.innerWidth < 768;
    this.setState({
      isMobile: isMobile,
      sidebarHidden: isMobile
    });
  };

  handlePageClick = page => this.setState({currentPage: page});

  handleCategoryChange = categoryId => {
    this.setState({
      currentPage: 1,
      filters: {
        ...this.state.filters,
        categoryId: categoryId
      }
    });
  };

  handlePriceClick = () => {
    const newState = {};
    const priceFromElement = document.getElementById('filterPriceFrom');
    const priceToElement = document.getElementById('filterPriceTo');

    if (priceFromElement.value) {
      const priceFrom = parseInt(priceFromElement.value, 10);

      if (priceFrom > 0)
        newState.priceFrom = priceFrom;
    }

    if (priceToElement.value) {
      const priceTo = parseInt(priceToElement.value, 10);

      if (priceTo > 0)
        newState.priceTo = priceTo;
    }

    if (!newState.priceFrom) priceFromElement.value = null;
    if (!newState.priceTo) priceToElement.value = null;

    if (!_.isEmpty(newState)) this.setState({
      currentPage: 1,
      filters: {
        ...this.state.filters,
        ...newState
      }
    });
  }

  handleSearchClick = () => {
    const name = _.trim(document.getElementById('filterName').value);
    if (name)
      this.setState({
        currentPage: 1,
        filters: {
          ...this.state.filters,
          name: name
        }
      });
  }

  handleSortChange = (value) => {
    const sort = _.trim(value);
    if (sort)
      this.setState({
        currentPage: 1,
        filters: {
          ...this.state.filters,
          sort: sort
        }
      });
  }

  resetFilters = () => {
    this.setState({
      filters: {
        categoryId: null,
        name: null,
        priceFrom: null,
        priceTo: null,
        sort: null
      }
    });
  }

  getProducts = () => {
    const params = _.reduce({page: this.state.currentPage, ...this.state.filters}, (acc, param, key) => {
      if (param)
        acc[_.snakeCase(key)] = param;

      return acc;
    }, {});

    api.getProducts(params)
      .then(response => {
        this.props.history.push({
          search: new URLSearchParams(params).toString()
        });

        this.setState({
          products: response.data.products,
          currentPage: response.data.current_page,
          lastPage: response.data.last_page
        })
      });
  }

  componentDidMount() {
    window.addEventListener("resize", this.handleResize);
    this.handleResize();

    if (this.props.categories.length === 0)
      api.getCategories().then(({data}) => this.props.getCategories(data));

    this.getProducts();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (!_.isEqual(prevState.filters, this.state.filters) || prevState.currentPage !== this.state.currentPage)
      this.getProducts();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize)
  }

  render() {
    return (
      <main className="d-flex">
        <Sidebar
          filters={this.state.filters}
          categories={this.props.categories}
          hidden={this.state.sidebarHidden}
          handleCategoryChange={this.handleCategoryChange}
          handlePriceClick={this.handlePriceClick}
          handleSearchClick={this.handleSearchClick}
          handleSortChange={this.handleSortChange}
          resetFilters={this.resetFilters}
        />

        <div className="w-100">
          {
            this.state.isMobile
              ? (
                <button onClick={this.handleToggleFiltersClick} className="btn btn-outline-dark m-2">
                  {this.state.sidebarHidden ? 'ФИЛЬТРЫ' : '<'}
                </button>
              ) : null
          }
          <ShopProducts
            products={this.state.products}
            handlePageClick={this.handlePageClick}
            currentPage={this.state.currentPage}
            lastPage={this.state.lastPage ? this.state.lastPage : 1}
          />
        </div>
      </main>
    );
  }
}

export default connect(
  state => ({
    categories: state.categories
  }),
  dispatch => ({
    getCategories: categories => dispatch(getCategories(categories))
  })
)(ShopContainer);

