import React from 'react';
import {connect} from 'react-redux';
import * as api from '../api';
import {edit_cart, get_local, CART_PRODUCTS_KEY, CART_KEY} from "../helpers";
import {getCookie} from "../cookie";

// REDUX ACTIONS
import {removeCartProduct, setCart} from "../Redux/Actions";

// COMPONENTS
import Cart from "../Components/Cart";

class CartContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
      orderInProcess: false,
      order: null
    };
  }

  handleCountChange = (product, count) => {
    edit_cart(product, count);

    const products = this.state.products.reduce((acc, item) => {
      if (item.id === product.id) {
        if (count === 0) return acc;
        item.count = count;
      }

      acc.push(item);

      return acc;
    }, []);

    if (!products.find(item => item.id === product.id)) {
      this.props.removeFromCart(product.id);
    }

    this.setState({
      products: products
    });
  };

  handleBuyClick = () => {
    if (this.state.products.length === 0) {
      alert('Корзина пуста');
      return;
    }

    if (this.props.auth) {
      this.setState({orderInProcess: true});
      return api.createOrder().then(response => {
        this.setState({
          orderInProcess: false,
          products: [],
        });
        this.props.flushCart();
        this.orderNotify(response.data.id, response.data.price, this.props.email);
      });
    }

    const email = document.getElementById('email').value.trim();

    if (!email) return;

    this.setState({orderInProcess: true});

    const order = this.state.products.map(product => ({
      product_id: product.id,
      count: product.count
    }));

    return api.createOrder(order, email.toLowerCase()).then(response => {
      localStorage.setItem(CART_PRODUCTS_KEY, []);
      localStorage.setItem(CART_KEY, []);

      this.setState({
        orderInProcess: false,
        products: [],
      });

      this.props.flushCart();
      this.orderNotify(response.data.id, response.data.price, email);
    });
  };

  orderNotify = (id, price, email) => alert(
    `Заказ №${id} стоимостью ${price} Р оформлен. Подробности отправлены на почту ${email}`
  );

  componentDidMount() {
    if (!!getCookie('token'))
      api.getCart().then(response => this.setState({
        products: response.data
      }));
    else this.setState({
      products: get_local(CART_PRODUCTS_KEY)
    })
  }

  render() {
    return <Cart
      auth={this.props.auth}
      orderInProcess={this.state.orderInProcess}
      products={this.state.products}
      handleCountChange={this.handleCountChange}
      handleBuyClick={this.handleBuyClick}
    />;
  }
}

export default connect(
  state => ({
    auth: state.profile.authorized,
    email: state.profile.email,
  }),
  dispatch => ({
    removeFromCart: id => dispatch(removeCartProduct(id)),
    flushCart: () => dispatch(setCart([])),
  })
)(CartContainer);