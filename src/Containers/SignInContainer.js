import React from 'react';
import {connect} from 'react-redux';
import * as api from '../api';
import {get_local, CART_PRODUCTS_KEY, FAVOURITES_KEY} from "../helpers";

// REDUX ACTIONS
import {signIn} from "../Redux/Actions";

// COMPONENTS
import SignIn from "../Components/SignIn";
import {setCookie} from "../cookie";

class SignInContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      invalidCredentials: false
    };
  }

  clearStorageAndReload = () => {
    localStorage.clear();
    window.location.replace('/');
  }

  handleSignInClick = () => {
    const email = document.getElementById('email').value.trim();
    const password = document.getElementById('password').value.trim();
    if (email && password) {
      api.signIn(email.toLowerCase(), password)
        .then(({data}) => {
          setCookie('token', data.token);

          api.setToken(data.token);

          const cart = get_local(CART_PRODUCTS_KEY);
          const favourites = get_local(FAVOURITES_KEY);

          if (cart !== null && cart.length > 0) {
            api.editCart(cart.map(item => ({
              product_id: item.id,
              count: item.count,
            }))).then(() => {
              if (favourites !== null && favourites.length > 0) {
                api.setFavourite(favourites).then(() => this.clearStorageAndReload());
              } else {
                this.clearStorageAndReload();
              }
            });
          } else if (favourites !== null && favourites.length > 0) {
            api.setFavourite(favourites).then(() => this.clearStorageAndReload());
          } else {
            this.clearStorageAndReload();
          }
        })
        .catch(() => this.setState({invalidCredentials: true}));
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.profile.authorized)
      this.props.history.replace('/');
  }

  render() {
    return <SignIn
      handleSignInClick={this.handleSignInClick}
      invalidCredentials={this.state.invalidCredentials}
    />
  }
}

export default connect(
  state => ({
    profile: state.profile
  }),
  dispatch => ({
    signIn: data => dispatch(signIn(data))
  })
)(SignInContainer);