import React from 'react';
import {connect} from 'react-redux';

// COMPONENTS
import Header from "../Components/Header";
import {deleteCookie} from "../cookie";

const HeaderContainer = ({cartCount, favouritesCount, profile}) => (
  <Header
    cartCount={cartCount}
    favouritesCount={favouritesCount}
    profile={profile}
    signOut={() => {
      deleteCookie('token');
      window.location.replace('/');
    }}
  />
)

export default connect(
  state => ({
    profile: state.profile,
    cartCount: state.cart.length,
    favouritesCount: state.favourites.length
  })
)(HeaderContainer);