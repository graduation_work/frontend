import React from 'react';
import * as api from '../api';

import Orders from "../Components/Orders";

class OrdersContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      orders: [],
      currentPage: 1,
      lastPage: 1
    }
  }

  handlePageClick = page => {
    api.getOrders(page).then(response => this.setState({
      orders: response.data.data,
      lastPage: response.data.lastPage,
      currentPage: response.data.currentPage,
    }));
  }

  componentDidMount() {
    api.getOrders(this.state.currentPage).then(response => this.setState({
      orders: response.data.data,
      lastPage: response.data.lastPage
    }));
  }

  render() {
    return <Orders
      orders={this.state.orders}
      currentPage={this.state.currentPage}
      lastPage={this.state.lastPage}
      handlePageClick={this.handlePageClick}
    />;
  }
}

export default OrdersContainer;