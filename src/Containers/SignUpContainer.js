import React from 'react';
import {connect} from 'react-redux';

// REDUX ACTIONS
import {signIn} from "../Redux/Actions";

// COMPONENTS
import SignUp from "../Components/SignUp";
import * as api from "../api";
import {setCookie} from "../cookie";
import {CART_PRODUCTS_KEY, FAVOURITES_KEY, get_local} from "../helpers";

class SignUpContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: {}
    }
  }

  clearStorageAndReload = () => {
    localStorage.clear();
    window.location.replace('/');
  }

  handleSignUpClick = () => {
    const email = document.getElementById('email').value.trim();
    const password = document.getElementById('password').value.trim();
    const passwordConfirmation = document.getElementById('password_confirmation').value.trim();
    if (email && password && passwordConfirmation)
      api.signUp(email.toLowerCase(), password, passwordConfirmation)
        .then(({data}) => {
          setCookie('token', data.token);

          api.setToken(data.token);

          const cart = get_local(CART_PRODUCTS_KEY);
          const favourites = get_local(FAVOURITES_KEY);

          if (cart !== null && cart.length > 0) {
            api.editCart(cart.map(item => ({
              product_id: item.id,
              count: item.count,
            }))).then(() => {
              if (favourites !== null && favourites.length > 0) {
                api.setFavourite(favourites).then(() => this.clearStorageAndReload());
              } else {
                this.clearStorageAndReload();
              }
            });
          } else if (favourites !== null && favourites.length > 0) {
            api.setFavourite(favourites).then(() => this.clearStorageAndReload());
          } else {
            this.clearStorageAndReload();
          }
        })
        .catch(({response}) => {
          if (response.data.errors)
            this.setState({errors: response.data.errors})
        })
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.profile.authorized)
      this.props.history.replace('/');
  }

  render() {
    return <SignUp
      errors={this.state.errors}
      handleSignUpClick={this.handleSignUpClick}
    />;
  }
}

export default connect(
  state => ({
    profile: state.profile
  }),
  dispatch => ({
    signIn: data => dispatch(signIn(data))
  })
)(SignUpContainer);