import React from 'react';
import {connect} from 'react-redux';
import * as api from '../api';
import _ from 'lodash';
import {get_local, FAVOURITE_PRODUCTS_KEY} from "../helpers";

// COMPONENTS
import Favourites from "../Components/Favourites";

class FavouritesContainer extends React.Component {
  constructor(props) {
    super(props);

    const page = parseInt((new URLSearchParams(props.location.search)).get('page'), 10);

    this.state = {
      products: [],
      currentPage: page ? page : 1,
      lastPage: 1
    }
  }

  getFavourites = () => {
    api.getFavourites(this.state.currentPage).then(response => {
      this.props.history.push({
        search: `page=${response.data.current_page}`
      });
      this.setState({
        products: response.data.products,
        lastPage: response.data.last_page,
        currentPage: response.data.current_page
      });
    }).catch(() => {
      this.setState({
        products: get_local(FAVOURITE_PRODUCTS_KEY),
        lastPage: 1,
        currentPage: 1
      });
    });
  }

  handlePageClick = page => this.setState({currentPage: page});

  componentDidMount() {
    this.getFavourites();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.currentPage !== this.state.currentPage) this.getFavourites();
    const diff = _.difference(prevProps.favourites, this.props.favourites);
    if (diff.length === 1) {
      const id = diff[0];
      this.setState({
        products: this.state.products.filter(product => product.id !== id)
      })
    }
  }

  render() {
    return <Favourites
      products={this.state.products}
      handlePageClick={this.handlePageClick}
      currentPage={this.state.currentPage}
      lastPage={this.state.lastPage}
    />
  }
}

export default connect(state => ({favourites: state.favourites}))(FavouritesContainer);