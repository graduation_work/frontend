import {
  SET_FAVOURITES,
  SET_FAVOURITE_PRODUCT,
  UNSET_FAVOURITE_PRODUCT
} from "../Actions/favouritesActions";

const favourites = (state = [], action) => {
  switch (action.type) {
    case SET_FAVOURITES:
      return action.data;
    case SET_FAVOURITE_PRODUCT:
      return [
        ...state,
        action.id
      ];
    case UNSET_FAVOURITE_PRODUCT:
      return state.filter(id => id !== action.id);
    default:
      return state;
  }
};

export default favourites;