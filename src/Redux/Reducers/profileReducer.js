import {
  SIGNIN,
} from "../Actions/profileActions";

const profile = (state = {}, action) => {
  switch (action.type) {
    case SIGNIN:
      return {
        authorized: true,
        ...action.profile
      };
    default:
      return state;
  }
};

export default profile;