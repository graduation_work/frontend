import {combineReducers} from 'redux';
import * as reducers from './Reducers';

export default combineReducers(reducers);