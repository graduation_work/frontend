export const GET_ORDERS = 'GET_ORDERS';

export function getOrders(data) {
  return {
    type: GET_ORDERS,
    orders: data
  };
}