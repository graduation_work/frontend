import {getCategories} from "./categoriesActions";
import {signIn} from "./profileActions";
import {setCart, addCartProduct, removeCartProduct} from "./cartActions";
import {setFavourites, setFavourite, unsetFavourite} from "./favouritesActions";
import {getOrders} from "./ordersActions";

export {
  getCategories,
  signIn,setCart, addCartProduct, removeCartProduct,
  setFavourites, setFavourite, unsetFavourite,
  getOrders
}