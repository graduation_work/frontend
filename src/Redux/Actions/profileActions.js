export const SIGNIN = 'SIGNIN';

export function signIn(data) {
  return {
    type: SIGNIN,
    profile: data
  };
}