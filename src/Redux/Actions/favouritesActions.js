export const SET_FAVOURITES = 'SET_FAVOURITES';
export const SET_FAVOURITE_PRODUCT = 'SET_FAVOURITE_PRODUCT';
export const UNSET_FAVOURITE_PRODUCT = 'UNSET_FAVOURITE_PRODUCT';


export const setFavourites = data => ({
  type: SET_FAVOURITES,
  data: data
});

export const setFavourite = id => ({
  type: SET_FAVOURITE_PRODUCT,
  id: id
});

export const unsetFavourite = id => ({
  type: UNSET_FAVOURITE_PRODUCT,
  id: id
});