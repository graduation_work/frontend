import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import * as api from './api';
import {connect} from 'react-redux';
import {deleteCookie} from "./cookie";
import {get_local, CART_KEY, FAVOURITES_KEY} from "./helpers";

// REDUX ACTIONS
import {signIn, setCart, setFavourites} from "./Redux/Actions";

// CSS
import './Assets/css/bootstrap-extended.css'

// COMPONENTS AND CONTAINERS
import Header from "./Containers/HeaderContainer";
import Home from "./Containers/HomeContainer";
import ShopProducts from "./Containers/ShopContainer";
import Product from "./Containers/ProductContainer";
import Cart from "./Containers/CartContainer";
import Favourites from "./Containers/FavouritesContainer";
import SignIn from "./Containers/SignInContainer";
import SignUp from "./Containers/SignUpContainer";
import Orders from './Containers/OrdersContainer';
import NotFound from "./Components/NotFound";

class App extends React.Component {
  componentDidMount() {
    api.getProfile()
      .then(response => {
        this.props.getProfile(response.data.user);
        this.props.setCart(response.data.cart);
        this.props.setFavourites(response.data.favourites);
      })
      .catch(() => {
        deleteCookie('token');

        if (this.props.profile.authorized)
          window.location.replace('/');

        this.props.setCart(get_local(CART_KEY));
        this.props.setFavourites(get_local(FAVOURITES_KEY));
      });
  }

  render() {
    return (
      <BrowserRouter className="container-fluid d-flex flex-column">
        <Header/>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/shop" exact component={ShopProducts}/>
          <Route path="/product/:id" exact component={Product}/>
          <Route path="/cart" exact component={Cart}/>
          <Route path="/favourites" exact component={Favourites}/>
          <Route path="/orders" exact component={Orders}/>
          <Route path="/signin" exact component={SignIn}/>
          <Route path="/signup" exact component={SignUp}/>
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default connect(
  state => ({
    profile: state.profile,
    cart: state.cart,
    favourites: state.favourites,
  }),
  dispatch => ({
    getProfile: user => dispatch(signIn(user)),
    setCart: data => dispatch(setCart(data)),
    setFavourites: data => dispatch(setFavourites(data)),
  })
)(App);
